## Apache Oozie at the Cloudera Hadoop platform ##

At the BigData project I work on we schedule Spark jobs through the shell which is a widely used solution. Anyway when the workflow of your Spark jobs becomes more difficult then [Apache Oozie](https://oozie.apache.org/) is a tool you should be considering to use.
Through Oozie you can run:

    Email Action
    Shell Action
    Hive Action
    Hive 2 Action
    Sqoop Action
    Ssh Action
    DistCp Action
    Spark Action
    Git Action
    Writing a Custom Action Executor

Let's try the [Spark Action](https://oozie.apache.org/docs/5.1.0/DG_SparkActionExtension.html) in this demo, as the playground I borrowed our [CDH 5.11.2](https://www.cloudera.com/products/open-source/apache-hadoop/key-cdh-components.html) development cluster.    
(domains are changed of course :)

### Few tips before going into Oozie ###

#### Spark version ####

By default [Cloudera](https://www.cloudera.com/) comes with Spark 1.6 installed into Oozie. You can check it by listing the sharedlibs which oozie uses to start your actions:

```sh
NONPROD [root@devsx010 ~]# oozie admin -shareliblist -oozie http://devsx010.cz.nonprod:11000/oozie/
[Available ShareLib]
hive
distcp
mapreduce-streaming
spark
oozie
hcatalog
hive2
sqoop
pig
```

Now let's examine the spark libs linked into oozie regarding the spark version:

```sh
NONPROD [root@devsx010 ~]# oozie admin -sharelibupdate -oozie http://devsx010.cz.nonprod:11000/oozie/
[ShareLib update status]
	sharelibDirOld = hdfs://hdfsHA/user/oozie/share/lib/lib_20180719173210
	host = http://devsx010.cz.nonprod:11000/oozie
	sharelibDirNew = hdfs://hdfsHA/user/oozie/share/lib/lib_20180719173210
	status = Successful
```

This tells me that Oozie shared libs are located on the HDFS, so let's check them:

```sh
NONPROD [root@devsx010 ~]# hdfs dfs -ls /user/oozie/share/lib/lib_20180719173210/spark | grep spark-
-rwxrwxr-x   3 oozie oozie      33125 2018-07-19 17:32 /user/oozie/share/lib/lib_20180719173210/spark/oozie-sharelib-spark-4.1.0-cdh5.11.2.jar
-rwxrwxr-x   3 oozie oozie     103606 2018-07-19 17:32 /user/oozie/share/lib/lib_20180719173210/spark/spark-avro_2.10-1.1.0-cdh5.11.2.jar
-rwxrwxr-x   3 oozie oozie      46171 2018-07-19 17:32 /user/oozie/share/lib/lib_20180719173210/spark/spark-bagel_2.10-1.6.0-cdh5.11.2.jar
-rwxrwxr-x   3 oozie oozie    5271287 2018-07-19 17:32 /user/oozie/share/lib/lib_20180719173210/spark/spark-catalyst_2.10-1.6.0-cdh5.11.2.jar
-rwxrwxr-x   3 oozie oozie   11663061 2018-07-19 17:32 /user/oozie/share/lib/lib_20180719173210/spark/spark-core_2.10-1.6.0-cdh5.11.2.jar
-rwxrwxr-x   3 oozie oozie     655666 2018-07-19 17:32 /user/oozie/share/lib/lib_20180719173210/spark/spark-graphx_2.10-1.6.0-cdh5.11.2.jar
-rwxrwxr-x   3 oozie oozie    1331068 2018-07-19 17:32 /user/oozie/share/lib/lib_20180719173210/spark/spark-hive_2.10-1.6.0-cdh5.11.2.jar
-rwxrwxr-x   3 oozie oozie      68079 2018-07-19 17:32 /user/oozie/share/lib/lib_20180719173210/spark/spark-launcher_2.10-1.6.0-cdh5.11.2.jar
-rwxrwxr-x   3 oozie oozie      77273 2018-07-19 17:32 /user/oozie/share/lib/lib_20180719173210/spark/spark-lineage_2.10-1.6.0-cdh5.11.2.jar
-rwxrwxr-x   3 oozie oozie    4999835 2018-07-19 17:32 /user/oozie/share/lib/lib_20180719173210/spark/spark-mllib_2.10-1.6.0-cdh5.11.2.jar
-rwxrwxr-x   3 oozie oozie    2357547 2018-07-19 17:32 /user/oozie/share/lib/lib_20180719173210/spark/spark-network-common_2.10-1.6.0-cdh5.11.2.jar
-rwxrwxr-x   3 oozie oozie      51921 2018-07-19 17:32 /user/oozie/share/lib/lib_20180719173210/spark/spark-network-shuffle_2.10-1.6.0-cdh5.11.2.jar
-rwxrwxr-x   3 oozie oozie     688350 2018-07-19 17:32 /user/oozie/share/lib/lib_20180719173210/spark/spark-repl_2.10-1.6.0-cdh5.11.2.jar
-rwxrwxr-x   3 oozie oozie    4102540 2018-07-19 17:32 /user/oozie/share/lib/lib_20180719173210/spark/spark-sql_2.10-1.6.0-cdh5.11.2.jar
-rwxrwxr-x   3 oozie oozie      86096 2018-07-19 17:32 /user/oozie/share/lib/lib_20180719173210/spark/spark-streaming-flume-sink_2.10-1.6.0-cdh5.11.2.jar
-rwxrwxr-x   3 oozie oozie     106076 2018-07-19 17:32 /user/oozie/share/lib/lib_20180719173210/spark/spark-streaming-flume_2.10-1.6.0-cdh5.11.2.jar
-rwxrwxr-x   3 oozie oozie     291374 2018-07-19 17:32 /user/oozie/share/lib/lib_20180719173210/spark/spark-streaming-kafka_2.10-1.6.0-cdh5.11.2.jar
-rwxrwxr-x   3 oozie oozie    2060810 2018-07-19 17:32 /user/oozie/share/lib/lib_20180719173210/spark/spark-streaming_2.10-1.6.0-cdh5.11.2.jar
-rwxrwxr-x   3 oozie oozie      39527 2018-07-19 17:32 /user/oozie/share/lib/lib_20180719173210/spark/spark-unsafe_2.10-1.6.0-cdh5.11.2.jar
-rwxrwxr-x   3 oozie oozie     583158 2018-07-19 17:32 /user/oozie/share/lib/lib_20180719173210/spark/spark-yarn_2.10-1.6.0-cdh5.11.2.jar
```
as you can see Spark version linked into Oozie is 1.6. You can try following steps listed in this [blog](https://www.ericlin.me/2017/01/how-to-load-different-version-of-spark-into-oozie/) to install Spark2,    
anyway Cloudera support says this setup is not supported as of CDH 5.12. So better is to go with [CDH 6.0](https://www.cloudera.com/documentation/enterprise/6/release-notes/topics/rg_cdh_600_new_features.html) which should support Spark2 actions.

#### Memory requirments and problem tracking ####

I did my tests of Oozie at our development environment cluster with 12 nodemanagers everyone having multiple gigabytes of memory and cores. And I have NEVER encountered any problems with launching Spark jobs over Oozie.
Anyway, internet is full of people complaining about spark jobs being stuck after they start it from oozie. Most of these problems are caused by resource manager not having enough of resources to plan the user action, like [Job stuck in Accepted state](https://community.cloudera.com/t5/Batch-Processing-and-Workflow/JOB-Stuck-in-Accepted-State/td-p/29494) or
[Application get stuck at 95%](https://mapr.com/community/s/question/0D50L00006BItgXSAT/application-get-stuck-at-95). Guys please always check the logs of ResourceManager for explanation of why your action doesn't start.


#### Cloudera and Oozie workflow jobTracker parameter ####

When I started to work with oozie for the first time I was confused what value to use in the job-tracker parameter in the Spark action. Following page about [CDH components ports](https://www.cloudera.com/documentation/enterprise/5-14-x/topics/cdh_ig_ports_cdh5.html) is very usefull. 
Regarding job-tracker always go with **yarn.resourcemanager.address** value. Another way of discovering where your resource manager operates is by simple listing the apps which RM listens to:

```
NONPROD [root@devsx010 tomas.kloucek]# yarn application -list
19/07/05 16:01:21 INFO client.RMProxy: Connecting to ResourceManager at devsx011.cz.nonprod/<internal ip>:8032
```

### Running the Spark job through Oozie ###

Okay let's try some Spark action through oozie. First you need to prepare three files, in my case I've got:

```sh
-rw-r--r-- 1 root          root              244  4. čec 22.09 job.properties
-rw-r--r-- 1 root          root          1849374  4. čec 22.02 spark-examples_2.10-1.6.1.jar
-rw-r--r-- 1 root          root              662  4. čec 22.31 workflow.xml
```

With **job.properties** you just need to tell oozie where you workflow configuration is and what properties in it to use.

```sh
NONPROD [root@devx010 tomas.kloucek]# cat job.properties 
nameNode=hdfs://devsx011.cz.nonprod:8020
jobTracker=devsx011.cz.nonprod:8032
master=yarn-cluster
queueName=default
examplesRoot=spark-example
oozie.use.system.libpath=true
oozie.wf.application.path=${nameNode}/user/tomas.kloucek/workflow.xml
```

With **workflow.xml** you set the same configuration parameters like with the shell command spark-submit. Let's try doc example with SparkPi class.

```sh
NONPROD [root@devsx010 tomas.kloucek]# cat workflow.xml 
<workflow-app xmlns="uri:oozie:workflow:0.5" name="SparkPi">
	<start to="spark-node" />
	<action name="spark-node">
		<spark xmlns="uri:oozie:spark-action:0.1">
			<job-tracker>${jobTracker}</job-tracker>
			<name-node>${nameNode}</name-node>
			<master>${master}</master>
			<name>Spark-Pi</name>
			<class>org.apache.spark.examples.SparkPi</class>
			<jar>${nameNode}/user/tomas.kloucek/spark-examples_2.10-1.6.1.jar</jar>
			<arg>10</arg>
		</spark>
		<ok to="end" />
		<error to="fail" />
	</action>
	<kill name="fail">
		<message>Workflow failed, error message[${wf:errorMessage(wf:lastErrorNode())}] </message>
	</kill>
	<end name="end" />
</workflow-app>
NONPROD [root@devsx010 tomas.kloucek]# 
```

Now **spark-examples_2.10-1.6.1.jar** is a jar I built with sbt. With these three prerequisities ready you can try to run spark action over oozie!

```sh
NONPROD [root@devsx010 tomas.kloucek]# oozie job -oozie http://devsx010.cz.nonprod:11000/oozie/ -config job.properties -run
job: 0000003-190607135703825-oozie-oozi-W
```

Now there are multiple ways of checking the Spark task status, I use checking through yarn commandline tool with listing the running tasks:

```sh
NONPROD [root@devsx010 tomas.kloucek]# yarn application -list
19/07/05 23:58:32 INFO client.RMProxy: Connecting to ResourceManager at devsx011.cz.nonprod/<internal_ip>:8032
Total number of applications (application-types: [] and states: [SUBMITTED, ACCEPTED, RUNNING]):2
                Application-Id	    Application-Name	    Application-Type	      User	     Queue	             State	       Final-State	       Progress	                       Tracking-URL
application_1559652993352_1052	oozie:launcher:T=spark:W=SparkPi:A=spark-node:ID=0000003-190607135703825-oozie-oozi-W	           MAPREDUCE	tomas.kloucek	root.users.tomas_dot_kloucek	           RUNNING	         UNDEFINED	             5%	  http://devsx013.cz.nonprod:34611
```

```sh
NONPROD [root@devsx010 tomas.kloucek]# yarn application -list
19/07/05 23:58:48 INFO client.RMProxy: Connecting to ResourceManager at devsx011.cz.nonprod/<internal_ip>:8032
Total number of applications (application-types: [] and states: [SUBMITTED, ACCEPTED, RUNNING]):3
                Application-Id	    Application-Name	    Application-Type	      User	     Queue	             State	       Final-State	       Progress	                       Tracking-URL
application_1559652993352_1053	            Spark-Pi	               SPARK	tomas.kloucek	root.users.tomas_dot_kloucek	           RUNNING	         UNDEFINED	            10%	         http://10.129.129.17:43225
application_1559652993352_1052	oozie:launcher:T=spark:W=SparkPi:A=spark-node:ID=0000003-190607135703825-oozie-oozi-W	           MAPREDUCE	tomas.kloucek	root.users.tomas_dot_kloucek	           RUNNING	         UNDEFINED	            95%	  http://devsx013.cz.nonprod:34611
```

when the Spark task is done then both mapreduce launcher and the spark action should disappear from the list. Now let's verify that job is done through oozie.

```sh
NONPROD [root@devsx010 tomas.kloucek]# oozie job -oozie http://devsx010.cz.nonprod:11000/oozie/ -info 0000003-190607135703825-oozie-oozi-W 
Job ID : 0000003-190607135703825-oozie-oozi-W
------------------------------------------------------------------------------------------------------------------------------------
Workflow Name : SparkPi
App Path      : hdfs://devsx011.cz.nonprod:8020/user/tomas.kloucek/workflow.xml
Status        : SUCCEEDED
Run           : 0
User          : tomas.kloucek
Group         : -
Created       : 2019-07-05 21:58 GMT
Started       : 2019-07-05 21:58 GMT
Last Modified : 2019-07-05 21:58 GMT
Ended         : 2019-07-05 21:58 GMT
CoordAction ID: -

Actions
------------------------------------------------------------------------------------------------------------------------------------
ID                                                                            Status    Ext ID                 Ext Status Err Code  
------------------------------------------------------------------------------------------------------------------------------------
0000003-190607135703825-oozie-oozi-W@:start:                                  OK        -                      OK         -         
------------------------------------------------------------------------------------------------------------------------------------
0000003-190607135703825-oozie-oozi-W@spark-node                               OK        job_1559652993352_1052 SUCCEEDED  -         
------------------------------------------------------------------------------------------------------------------------------------
0000003-190607135703825-oozie-oozi-W@end                                      OK        -                      OK         -         
------------------------------------------------------------------------------------------------------------------------------------
```
...Or if you want to be more verbose about the Spark action:

```sh
NONPROD [root@devsx010 tomas.kloucek]# oozie job -oozie http://devsx010.cz.nonprod:11000/oozie/ -info 0000003-190607135703825-oozie-oozi-W@spark-node -verbose
ID : 0000003-190607135703825-oozie-oozi-W@spark-node
------------------------------------------------------------------------------------------------------------------------------------
Console URL       : https://devsx011.cz.nonprod:8090/proxy/application_1559652993352_1052/
Error Code        : -
Error Message     : -
External ID       : job_1559652993352_1052
External Status   : SUCCEEDED
Name              : spark-node
Retries           : 0
Tracker URI       : naczsx011.cz.nonprod:8032
Type              : spark
Started           : 2019-07-05 21:58:14 GMT
Status            : OK
Ended             : 2019-07-05 21:58:54 GMT
External Stats    : null
External ChildIDs : job_1559652993352_1053
------------------------------------------------------------------------------------------------------------------------------------
NONPROD [root@devsx010 tomas.kloucek]# 
```

### Howto find output from the Spark task running through Oozie ###

I did another oozie job run so let's get the details:

```sh
NONPROD [root@devsx010 tomas.kloucek]# oozie job -oozie http://devsx010.cz.nonprod:11000/oozie/ -info 0000007-190607135703825-oozie-oozi-W@spark-node -verbose
ID : 0000007-190607135703825-oozie-oozi-W@spark-node
------------------------------------------------------------------------------------------------------------------------------------
Console URL       : https://devsx011.cz.nonprod:8090/proxy/application_1559652993352_1066/
Error Code        : -
Error Message     : -
External ID       : job_1559652993352_1066
External Status   : SUCCEEDED
Name              : spark-node
Retries           : 0
Tracker URI       : naczsx011.cz.nonprod:8032
Type              : spark
Started           : 2019-07-06 20:51:46 GMT
Status            : OK
Ended             : 2019-07-06 20:52:50 GMT
External Stats    : null
External ChildIDs : job_1559652993352_1067
------------------------------------------------------------------------------------------------------------------------------------
NONPROD [root@devsx010 tomas.kloucek]# 
```

Now don't get confused! **application_1559652993352_1066** is a job id of the oozie mapreduce launcher starting your Spark task. To get the logs of the Spark action we need to query the yarn:

```sh
NONPROD [root@devsx010 tomas.kloucek]# yarn logs -applicationId application_1559652993352_1066 |grep "tracking URL"
19/07/06 23:28:25 INFO client.RMProxy: Connecting to ResourceManager at naczsx011.cz.nonprod/10.129.129.11:8032
	 tracking URL: https://devsx011.cz.nonprod:8090/proxy/application_1559652993352_1067/
	 tracking URL: https://devsx011.cz.nonprod:8090/proxy/application_1559652993352_1067/
	 tracking URL: https://devsx011.cz.nonprod:8090/proxy/application_1559652993352_1067/
```

URL https://devsx011.cz.nonprod:8090/proxy/application_1559652993352_1067/ is the endpoint with Spark logs. I found in them eventually:

```sh
.
.
2019-07-06 22:52:44,363 [dag-scheduler-event-loop] INFO  org.apache.spark.scheduler.DAGScheduler  - ResultStage 0 (reduce at SparkPi.scala:36) finished in 11.192 s
2019-07-06 22:52:44,364 [task-result-getter-3] INFO  org.apache.spark.scheduler.cluster.YarnClusterScheduler  - Removed TaskSet 0.0, whose tasks have all completed, from pool 
2019-07-06 22:52:44,372 [Driver] INFO  org.apache.spark.scheduler.DAGScheduler  - Job 0 finished: reduce at SparkPi.scala:36, took 11.587949 s
Pi is roughly 3.14145536
```

That's it. I hope you found my Oozie tutorial usefull. 

Best Regards

Tomas


